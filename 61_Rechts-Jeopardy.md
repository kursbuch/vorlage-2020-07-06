61 | Rechts-Jeopardy
=======================

**Thema:** Einführungsseminar

**Abstract:** In einem Gameshow-Format werden die rechtlichen Grundlagen der Freiwilligendienste thematisiert. Das Spiel kann im Wettbewerbsformat gespielt werden.

**Ziele:**

* Informieren zu Rechten und Pflichten im Freiwilligendienst

**Autor_innen:** Tobias Weiske

Beschreibung
------------

* Das Rechts-Jeopardy! ist aufgebaut wie normales Quiz: Es werden für die vier Kategorien mit ihren jeweils sechs Fragemöglichkeiten Antworten gesucht.
* In den Kategorien „Vertrag", „Krankheit", „Arbeit" und „Geld" können jeweils Punkte in Höhe von 5, 10, 15, 20, 25 oder 30 gewonnen werden.
* Es gibt Aktionsfelder mit unterschiedlichen Aufgaben.
* Der Spielleiter (Teamer) liest die angeforderten Fragen vor, kontrolliert oder berichtigt die Antworten und notiert die Punkte.

### Fragenübersicht

#### WAS IST WENN...?

**5 | In meiner nächsten Seminarwoche findet die große Nikolaus-Feier statt. Gibt es eine Möglichkeit daran trotz Seminar teilzunehmen?**

* Nein. Die Seminartage sind in vollem Umfang abzuleisten und gehören zum gesetzlich geregelten Dienst.

**10 | Aktion** 

* Aktion: Pantomime: Fußpilz

**15 | Ich habe in der Woche ein Bewerbungsgespräch für eine Ausbildung. Kann ich da einfach hingehen? Was muss ich bedenken?**

* Die ES muss die FW nicht freistellen! Ggf. muss der Termin verschoben werden.
* In der Regel wird die ES eine gute Lösung finden, die Freistellung kann dann schriftlich vereinbart werden (Es gibt eine Vorlage online)
* Der Fachbereich ist ebenfalls zu informieren!
* (Während der Freistellung gelten die privaten Versicherungen, nicht die dienstlichen!)

**20 | Was ist zu tun, wenn Du dich bei der Arbeit in der Einsatzstelle verletzt?**

* Vorgesetzte_n, Anleiter_in oder Kolleg_in informieren
* Ggf. Krankenwagen rufen (lassen).
* Der Vorfall muss dokumentiert werden, hierfür gibt es ein spezielles Buch oder einen speziellen Ordner. Dort werden auch Zeugen vermerkt.
* Je nach Verletzung ggf. sofort oder nach der Arbeit zum Arzt oder ins Krankenhaus gehen.
* Beim Arzt/ Im Krankenhaus sofort angeben, dass es sich um einen Arbeitsunfall handelt. Der Versicherungsschutz läuft nicht über die Krankenkasse, sondern über die Berufsgenossenschaft der Einsatzstelle.

**25 | Was tut man, wenn man während seiner Dienstzeit in der Einsatzstelle krank wird? Wen muss man wann informieren? Was ist mit der AUB (Arbeitunfähigkeitsbescheinigung)?**

* Die ES ist sofort zu informieren, spätestens zum Dienstantritt (Weiß jeder, wo er anrufen muss?).
* Bei einem Arztbesuch ist die ES zeitnah über den weiteren Verlauf zu informieren.
* Unterschiedliche Absprachen in den ES: Ab dem 1. oder 3. Tag der Krankheit ist eine Arbeitsunfähigkeitsbeschreibung vom Arzt in der ES abzugeben.
    * Die AUB ist der Nachweis für Arbeitgeber und Krankenkasse (muss auch dort eingereicht werden).
    * Dafür sorgen, dass eine Kopie der AUB an den Fachbereich geschickt wird.
* (Man meldet sich am Ende der Krankheit (spätestens am letzten Tag der Krankschreibung) in der ES und berichtet über einen möglichen letzten Arztbesuch oder erkundigt sich, ob man „normal“ wiederkommen kann (Welche Schicht etc.?)
 
**30 |	Was tut man, wenn man unmittelbar vor dem Seminar krank wird/ist? Wen muss man wann informieren? Was ist mit der AUB (Arbeitunfähigkeitsbescheinigung)?**

* Sowohl Fachbereich, als auch ES sofort über Krankheit informieren. Auch über den Anrufbeantworter oder eine schriftliche Nachricht.
* Ab dem 1. Tag der Krankheit ist eine Arbeitsunfähigkeitsbeschreibung vom Arzt sowohl beim Fachbereich, als auch bei der ES abzugeben.
    * Der Fachbereich erhält das Original
    * Die ES eine Kopie
    * Die AUB ist der Nachweis für Arbeitgeber und Krankenkasse (muss auch dort eingereicht werden).
    * Dafür sorgen, dass eine Kopie der AUB an den Fachbereich geschickt wird.
* (Während des Seminars: - Team informieren und Dringlichkeit besprechen; 
* ab dem 1.Krankheitstag ist die Krankschreibung dem Fachbereich vorzulegen, 
* das Team fährt FW ggf. zum Arzt)
* („Gesundmeldung“ auf Seminar: Der FW muss zum Seminar nachreisen, wenn die „Gesundmeldung“ erfolgt ist (in Absprache mit der Referentin))

#### VERTRAG

**5 | Nennt die Vertragspartner im FSJ und im BFD.**

* die Einsatzstelle
* der/die Freiwillige und ggf. sein gesetzlicher Vertreter bei Minderjährigen
* der Träger -> Erzbistum Hamburg  / der Generalvikar Ansgar Thim
* [nur BFD:]das Bundesamt für Familie und zivilgesellschaftliche Aufgaben (BAFzA) in Köln

**10 | Wie lange dauert die Probezeit in der Regel? Welche Kündigungsfristen gelten in und nach der Probezeit?**

* 6 Wochen dauert die Probezeit
* Innerhalb der Probezeit: 2 Wochen Kündigungsfrist
* Außerhalb der Probezeit: Einen Monat, jeweils zum Ende oder zum 15.

**15 | Welche Verpflichtung ist der/die Freiwillige eingegangen? Nenne mindestens 3 Stück:**

* Teilnahme an den Bildungsseminaren des Fachbereichs Freiwilligendienste
* Anleitergespräche und Reflexionsgespräche mit dem Fachbereich
* Mitarbeit in der Einsatzstelle und Mittragen der katholischen Ausrichtung
* Datenschutz beachten und Schweigepflicht einhalten
* Meldung bei Arbeitsunfähigkeit 
* Dienst- & Hausordnung der ES beachten und ggf. Kleiderordnung einhalten
* Ggf. ärztliche Untersuchung vor Dienstbeginn
* Erweitertes Führungszeugnis vor Dienstbeginn einreichen

**20 |Aktion**

*  Aktion: Stelle Grashüpfer mit zusammengebundenen Händen pantomimisch dar.

**25 | Welche Verpflichtung ist die Einsatzstelle laut Vertrag eingegangen? Nenne mindestens 3 Stück:**

* die ES stellt den materiellen Rahmen (Taschengeld, Verpflegung, ggf. Wohnung, Zuschuss zur Unterkunft/Fahrkarten zur ES)
* sie kümmert sich um die Anleitung des/der FW als eine zusätzliche Hilfskraft -> es gibt eine_n Anleiter_in! 
* stellt das Arbeitsfeld und die zur ES gehörenden Berufsfelder
* sie zahlt die Sozialversicherungsbeiträge einschl. der gesetzlichen Unfallversicherung

**30 | Auf wieviel Urlaub hat der/die Freiwillige Anspruch? Wann darf kein Urlaub genommen werden? 
Gibt es Unterschiede im Alter? Gibt es Sonderurlaub? Wenn ja, wann oder warum oder wie lange?**

* Alle Freiwilligen haben bei einem Dienst über 12 Monate 30 Tage Urlaub
* Während der Seminare kann kein Urlaub genommen werden. Ebenso kann die ES einen Teil der Urlaubstage festschreiben, bspw. während der Schließzeiten.

* (Gesetzliche Urlaubsregelungen:
    * Mindestens 26 Werktage Urlaub (nicht während der Seminare)
    * Minderjährige unter 16 (zu Beginn des Kalenderjahres) mind. 30 Werktage
    * Minderjährige unter 17 mind. 27 Werktage
    * Minderjährige unter 18 mind. 25 Werktage)
* Bei JuLeiCa-Besitzern ist ein Sonderurlaub zur Betreuung von Freizeiten in Absprache möglich (max. 10 Tage mit einem Eigenanteil an Urlaubstagen) 
* In Ausnahmen und mit Absprache in der ES sind weitere Tage möglich, an denen beispielsweise Gerichtstermine o.ä. wahrgenommen werden müssen (muss belegt werden)

#### ARBEIT

**5 | Wieviel beträgt die Arbeitszeit bei Volljährigen, welche Pausenregelung gelten und was ist bei Arbeitszeiten am Wochenende zu beachten?**

* Durchschnittlich 39 Stunden/Woche (jeweils die übliche Vollzeit in den ES, das variiert)
* Möglichst nicht zwei Wochenenden hintereinander
* Möglichst nicht an den Wochenenden vor und nach den Seminaren
* Tägl. 30 Minuten Pause, davon mindestens 1x15 Minuten

**10 | Welche Regelungen gelten für minderjährige Freiwillige bei den Pausen und der maximalen Arbeitszeit?**

* Maximal 8 Stunden pro Tag und 40 Stunden pro Woche
* Man darf nicht vor 6:00 Uhr und nach 20:00 Uhr arbeiten
* 60 Minuten Pause täglich, davon mindestens 1x30 Minuten 

**15 | Wieviel darf man am Stück arbeiten? Gibt es Unterschiede zwischen volljährigen und minderjährigen Freiweilligen? Wenn ja, welche? (Beachtet die unterschiedlichen Einrichtungsarten)**

* Volljährig: 5-Tage-Regelung in Einrichtungen wie Kita o.ä.
    * bei Pflegeeinrichtungen gilt eine 6-Tage-Woche
    * Hinweis: 12 Tage am Stück sind möglich (Mo-So + Mo-Fr), danach muss frei sein
* Minderjährig: 5Tage/Woche darf gearbeitet werden
    * max. 10 Tage am Stück (Mi bis So und Mo bis Fr)

**20 | Welche Schichten/ Arbeitszeiten sind im Freiwilligendienst, unabhängig vom Alter, unzulässig?**

* Die Nachtschicht!
* In Sonderfällen bedarf es eine Zusatzvereinbarung, um diese Arbeit kennenlernen zu dürfen (Ausnahme!), z-B. in der Bahnhofsmission (mit Zusatzvertrag)

**25 | Ist ein Nebenjob erlaubt? Wenn ja, was muss man beachten? Müssen Träger und Einsatzstelle davon erfahren?** 

* Im Freiwilligendienst ist die volle Arbeitskraft der Einsatzstelle zur Verfügung zu stellen, daher muss über Nebentätigkeiten informiert werden
* Der Träger hält Rücksprache mit der Einsatzstelle und kann dann Nebenjobs genehmigen
* Vorher muss eine Erlaubnis der Einsatzstelle eingeholt werden (schriftlich festgehalten: Was, wie lange gearbeitet wird)

**30 | Aktion**

* Aktion: Während du unentwegt auf einem Bein im Kreis hüpfst, erkläre „Gitterbett“ 

#### GELD

**5 | Welche gesetzlichen Ansprüche bleiben während des Freiwilligendienstes nach der Schulzeit bestehen?**

* Kindergeld
* Halb-/Waisenrente
* (Ggf. Wohngeld)

**10 | Aktion**

* Aktion: Beschreibe Kindergarten ohne die Wörter: klein, spielen, Schule und Erzieher

**15 | Was zahlen Einsatzstellen monatlich an Freiwillige ohne Unterkunft?**

* Taschengeld (402 €)
* Unterkunftszuschuss (100 €, einzelne Einsatzstellen zahlen auch 235 €)
* Beiträge zu den gesetzlichen Sozialversicherungen in vollem Umfang (Kranken-, Renten-, Pflege-, Unfall- und Arbeitslosenversicherung)
* Eine Berufsunfallversicherung wird für den FW abgeschlossen

* Insg. ohne Unterkunft: 502 €

**20 | Was zahlen Einsatzstellen monatlich an Freiwillige mit Unterkunft?**

* Taschengeld (402 €)
* Beiträge zu den gesetzlichen Sozialversicherungen in vollem Umfang (Kranken-, Renten-, Pflege-, Unfall- und Arbeitslosenversicherung)
* Eine Berufsunfallversicherung wird für den FW abgeschlossen

* Insg. mit Unterkunft: 402 €
* Die gestellt Unterkunft taucht als Sachbezugswert (235 €) auf

**25 | Aktion**

* Aktion: Nennt fünf Freiwillige aus den anderen Kleingruppen inkl. deren Einrichtungsart.

**30 | Bekommen Freiwillige das in jedem Fall das Taschengeld vom Arbeitgeber gezahlt, wenn sie krank werden? Wie sieht es in der Probezeit und nach der Probezeit aus?**

* Probezeit:
    * die ES müssen die Kosten nicht übernehmen, sondern verweisen die FW an ihre Krankenkasse. Streng genommen besteht der Anspruch auf Entgeltfortzahlung im Krankheitsfall nach vierwöchiger ununterbrochener Arbeit.
* Nach der Probezeit:
    * bis zur Dauer von 6 Wochen werden Taschengeld und Sachleistungen weitergezahlt
    * bei einer Krankheit (es muss die gleiche sein, wenn man zwischendurch wieder bei der Arbeit und gesund war addieren sich auch einzelne Zeiten), die länger dauert, übernimmt die Krankenversicherung die gesetzlich geregelte Leistung (=70 Prozent des Bruttoverdienstes, aber nicht mehr als 90 Prozent vom Netto-Gehalt, im FWD Brutto = Netto)

Quellen
-------

Die Sachbezugswerte wurden für 2020 aktualisiert, da das Verpflegungsgeld wegfällt, wurden die Fragen unter GELD umgestellt.

Das Rechts-Jeopardy hat bereits eine lange Tradition im Fachbereich Freiwilligendienste. Die ursprünglichen Autor_innen sind unbekannt, die letzte umfassende Überarbeitung fand 2017 durch Tobias Weiske statt.


Anhang
------

Hier alle Anhänge (z.B. Arbeitsblätter, Kopien oder Materialien auflisten)

* [Jeopardy-Theme](https://cloud.kjh.de/index.php/f/292465)
* [Kopiervorlage Fragekarten](https://cloud.kjh.de/index.php/f/292464)

Lizenz
------

**CC BY 4.0**  

![CC BY 4.0](https://raster.shields.io/badge/Lizenz-CC%20BY%204.0-orange)  
Weiternutzung als OER ausdrücklich erlaubt: Dieses Werk und dessen Inhalte sind - sofern nicht anders angegeben - lizenziert unter CC BY 4.0. Nennung gemäß TULLU-Regel bitte wie folgt: "Rechts-Jeopardy" von Tobias Weiske, Lizenz: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.de)  
Der Lizenzvertrag ist hier abrufbar: [https://creativecommons.org/licenses/by/4.0/deed.de](https://creativecommons.org/licenses/by/4.0/deed.de)
Das Werk wird herausgegeben von der [Jugend im Erzbistum Hamburg](https://jugend-erzbistum-hamburg.de).

