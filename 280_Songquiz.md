280 | Songquiz
=======================

**Thema:** Flexibel

**Abstract:** Die bekommen Musik vorgespielt und raten, wessen aktuelles Lieblingslied das ist. So kann zu Beginn einer Einheit oder nach einer Pause eine positive Atmosphäre geschaffen und gleichzeitig ein weiteres Kennelernen ermöglicht werden.

**Ziele:** Positive Stimmung schaffen und Kennenlernen

**Autor_innen:** Tobias Weiske

Beschreibung
------------

Zu Beginn erhalten alle TN Zettel und Stift. Alle schreiben ihren aktuellen Lieblingssong gut leserlich auf den Zettel und falten ihn einfach. Alle Zettel werden in einem Behälter gesammelt und durchmischt. Nacheinander werden die Songs vorgelesen oder (noch viel besser) vorgespielt. Dafür ist dann natürlich eine Internetverbindung notwendig.

Anschließend raten alle TN wessen Lieblingssong das sein könnte und legen sich gemeinsam fest.

Die Einheit ist nur mit kleinen Gruppen durchführbar, sonst dauert es zu lange. Die Diskussion sollte zugelassen und gefördert werden. So ist ein intensiveres Kennenlernen möglich und die TN erhalten die Gelegenheit sich auszutauschen.

Quellen
-------

-/-


Anhang
------

-/-

Lizenz
------

**CC0 1.0**  

![CC0 1.0](https://raster.shields.io/badge/Lizenz-CC0%201.0-orange)  
Weiternutzung als OER ausdrücklich erlaubt: Für dieses Werk wird kein urheberrechtlicher Schutz beansprucht, Freigabe unter CC0/Public Domain. Optionaler Hinweis gemäß TULLU-Regel: "Songquiz" von [Tobias Weiske](https://jugend-erzbistum-hamburg.de), freigegeben als: [CC0/Public Domain](https://creativecommons.org/publicdomain/zero/1.0/deed.de).
Das Werk wird herausgegeben von der [Jugend im Erzbistum Hamburg](https://jugend-erzbistum-hamburg.de).


