# README

## Wozu?

Mit der Textauszeichnungssprache markdown können wir Inhalte und Materialien von der Abhängigkeit von Textverarbeitungssoftware und entsprechenden Unternehmen befreien. markdown lässt sich mit dem freien Parser pandoc in viele andere Formate zur weiteren Verarbeitung oder in ein Ausgabeformat umwandeln (z.B. html oder pdf). Pandoc muss installiert werden und benötigt weitere Pakete, um in vollem Umfang zu funktionieren. Dann lassen sich über das Terminal verschiedene Befehle ausführen. Um diese Abhängigkeit von einer lokalen Installation zu umgehen und endgeräteunabhängiger arbeiten zu können, können wir dieses Git nutzen.

## Los gehts

Verstehe dieses Git als einen Zwischenschritt zu deinem fertigen Dokument. Du kannst dieses Git einfach klonen und dann deine Dokumente einfügen. Wir starten hier:

![Startumgebung](startumgebung.jpg)

Den help-Ordner hast du schon gefunden, er dient der Information und hat sonst keine Funktion. Die anderen beiden Dateien sagen dem Runner was er tun soll. In beiden Dokumenten musst du kleine Anpassungen vornehmen.

## .gitlab-ci.yml

Diese Datei sagt dem Runner, was er überhaupt tun soll. In dieser Vorlage sieht der Code so aus:

![CI anpassen 1](ci-anpassen_1.JPG)

Du musst zwei Dinge anpassen. Du muss dem Runner sagen, wo sich dein Git befindet und du musst pandoc mitteilen, in welchem Verzeichnis wir arbeiten wollen. Die https-Adresse des Git findest du über die Übersichtsseite deines Projektes unter der Schaltfläche "Clone". Dort kannst du die Adresse kopieren und in der .gitlab-ci.yml einfügen. Achtung: Das https:// steht bereits schon da (am Beginn der Zeile), du lässt alles bis zum @-Zeichen stehen.

![CI anpassen 2](ci-anpassen_2.JPG)

Als letztes musst du noch Pandoc sagen, in welchem Verzeichnis sich deine md-Dateien befinden. Hier gibst du das oberste Verzeichnis an.

## .pandoc-config.yml

![Pandoc konfigurieren](pandoc-konfigurieren.JPG)

In der .gitlab-ci.yml teilen wir Pandoc mit, dass wir diese Datei mit den dort aufgelisteten Attributen als Filter bei der Konvertierung verwenden möchten. Du musst hier nur Titel und Untertitel anpassen. Dann passt auch das Deckblatt in deiner Ausgabedatei.

## Das war es schon

Wenn du das gemacht hast, kannst du deine markdown-Dateien und dort verwendete inline-Bilder in den Hauptordner hochladen. Pandoc bestimmt die Reihenfolge anhand der Dateinamen. Benenne die Dateien also so um, dass sie korrekt sortiert werden (z.B. indem du 01_..., 02_... voranstellst).
Das Erzeugen des Dokuments nimmt etwas Zeit in Anspruch, weil erstmal alle Komponenten geladen werden müssen. Du kannst den Fortschritt unter CI/CD nachverfolgen. Dort findest du auch Fehlermeldungen oder das Ergebnis.
Der Runner läuft übrigens per default jedes Mal los, wenn du eine Datei änderst oder neu hinzufügst.

Das Ergebnis sieht dann z.B. so aus: [Beispiel-Ausgabedokument](Beispiel.pdf)

# Quellen und Shoutout

Als Vorlagen dienen:

* https://oa-pub.hos.tuhh.de/de/2019/08/19/kollaborativ-buecher-schreiben-mit-dem-gitlab-wiki/
* https://insights.tuhh.de/de/blog/tutorials/2018/10/04/gitlab-als-cms-einsetzen/

Das Docker-Image xldrkp/pandoc stammt ebenfalls von der TUHH

Dank, shoutout und props an Axel Dürkop und Florian Hagen!

![thanks](thanks.gif)
